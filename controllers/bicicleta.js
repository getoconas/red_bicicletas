var Bicicleta = require('../models/bicicleta');
const bicicletaCtrl = { }

bicicletaCtrl.getBicicletas = (req, res) => {
  res.render('bicicletas/index', {
    bicis: Bicicleta.allBicis
  })
}

bicicletaCtrl.createBicicletaGet = (req, res) => {
  res.render('bicicletas/create');
}

bicicletaCtrl.infoBicicleta = (req, res) => {
  var bici = Bicicleta.findById(req.params.id);
  res.render('bicicletas/info', { bici });
}

bicicletaCtrl.createBicicletaPost = (req, res) => {
  var bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
  bici.ubicacion = [req.body.lat, req.body.lng];
  Bicicleta.add(bici);

  res.redirect('/bicicletas');
}

bicicletaCtrl.updateBicicletaGet = (req, res) => {
  var bici = Bicicleta.findById(req.params.id);
  res.render('bicicletas/update', { bici });
}

bicicletaCtrl.updateBicicletaPost = (req, res) => {
  var bici = Bicicleta.findById(req.params.id);
  bici.id = req.body.id;
  bici.color = req.body.color;
  bici.modelo = req.body.modelo;
  bici.ubicacion = [req.body.lat, req.body.lng];

  res.redirect('/bicicletas');
}

bicicletaCtrl.deleteBicicleta = (req, res) => {
  Bicicleta.removeById(req.body.id);
  res.redirect('/bicicletas');
}

module.exports = bicicletaCtrl;

/*exports.bicicleta_list = function(req, res) {
  res.render('bicicletas/index', {
    bicis: Bicicleta.allBicis
  })
}

exports.bicicleta_create_get = function(req, res) {
  res.render('bicicletas/create');
}

exports.bicicleta_info_get = function(req, res) {
  var bici = Bicicleta.findById(req.params.id);
  res.render('bicicletas/info', { bici });
}

exports.bicicleta_create_post = function(req, res) {
  var bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
  bici.ubicacion = [req.body.lat, req.body.lng];
  Bicicleta.add(bici);

  res.redirect('/bicicletas');
}

exports.bicicleta_update_get = function(req, res) {
  var bici = Bicicleta.findById(req.params.id);
  res.render('bicicletas/update', { bici });
}

exports.bicicleta_update_post = function(req, res) {
  var bici = Bicicleta.findById(req.params.id);
  bici.id = req.body.id;
  bici.color = req.body.color;
  bici.modelo = req.body.modelo;
  bici.ubicacion = [req.body.lat, req.body.lng];

  res.redirect('/bicicletas');
}

exports.bicicleta_delete_post = function(req, res) {
  Bicicleta.removeById(req.body.id);
  res.redirect('/bicicletas');
}*/