var Bicicleta = function(id, color, modelo, ubicacion) {
	this.id = id;
	this.color = color;
	this.modelo = modelo;
	this.ubicacion = ubicacion;
}

Bicicleta.prototype.toString = function () {
	return 'id: ' + this.id + " | color " + this.color;
}

Bicicleta.allBicis = [];
Bicicleta.add = function(aBici) {
	Bicicleta.allBicis.push(aBici);
}

// Busca si existe una bicicleta por ID
Bicicleta.findById = function(aBiciId) {
	var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
	if (aBici)
		return aBici;
	else
		throw new Error(`No existe una bicicleta con el id ${aBiciId}`);
}

// Elimina una bicicleta por ID
Bicicleta.removeById = function(aBiciId) {
	for(var i = 0; i < Bicicleta.allBicis.length; i++) {
		if(Bicicleta.allBicis[i].id == aBiciId) {
			Bicicleta.allBicis.splice(i, 1);
			break;
		}
	}
}

/*
var a = new Bicicleta(1, 'rojo', 'urbana', [-34.6012424, -58.3861497]);
var b = new Bicicleta(2, 'blanca', 'urbana', [-34.596932, -58.3808287]);
var c = new Bicicleta(3, 'negra', 'montaña', [-34.5806424, -58.406987]);
var d = new Bicicleta(4, 'verde', 'urbana', [-34.597663, -58.394531]);

Bicicleta.add(a);
Bicicleta.add(b);
Bicicleta.add(c);
Bicicleta.add(d);
*/

module.exports = Bicicleta;