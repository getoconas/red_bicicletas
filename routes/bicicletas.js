var express = require('express');
var router = express.Router();
var bicicletaCtrl = require('../controllers/bicicleta');

router.get('/', bicicletaCtrl.getBicicletas);
router.get('/create', bicicletaCtrl.createBicicletaGet);
router.post('/create', bicicletaCtrl.createBicicletaPost);
router.get('/:id/info', bicicletaCtrl.infoBicicleta);
router.get('/:id/update', bicicletaCtrl.updateBicicletaGet);
router.post('/:id/update', bicicletaCtrl.updateBicicletaPost);
router.post('/:id/delete', bicicletaCtrl.deleteBicicleta);

module.exports = router;