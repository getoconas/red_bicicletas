var express = require('express');
var router = express.Router();
var bicicletaCtrlAPI = require('../../controllers/api/bicicletaControllerAPI');

router.get('/', bicicletaCtrlAPI.bicicleta_list);
router.post('/create', bicicletaCtrlAPI.bicicleta_create);
router.delete('/delete/:id', bicicletaCtrlAPI.bicicleta_delete);
router.put('/update/:id', bicicletaCtrlAPI.bicicleta_update);

module.exports = router;