# Proyecto Red de Bicicletas

## Instalación

Usar los siguientes comandos para la descarga e instalación del proyecto.

## Clonar el repositorio

```
git clone https://getoconas@bitbucket.org/getoconas/red_bicicletas.git
```

## Instalar las dependencias
```
npm install
```

## Una vez instalado las dependencias, levantar el proyecto

```
npm run devstart
```

## URL para la API de Bicicletas

| Acción | URL |
| ------ | ------ |
| Listar (get) | http://localhost:3000/api/bicicletas |
| Crear (post) | http://localhost:3000/api/bicicletas/create |
| Eliminar (delete) | http://localhost:3000/api/bicicletas/delete/id |
| Actualizar (put) | http://localhost:3000/api/bicicletas/update/id |

