var Bicicleta = require('../../models/bicicleta');

beforeEach( () => {
  Bicicleta.allBicis = [];
});

describe('Bicicleta.allBicis', () => {
  it('Comienza vacía', () => {
    expect(Bicicleta.allBicis.length).toBe(0);
  });
});

describe('Bicicleta.add', () => {
  it('Agregamos una', () => {
    expect(Bicicleta.allBicis.length).toBe(0);
    var a = new Bicicleta(1, 'rojo', 'urbana', [-34.6012424, -58.3861497]);
    Bicicleta.add(a);
    expect(Bicicleta.allBicis.length).toBe(1);
    expect(Bicicleta.allBicis[0]).toBe(a);
  });
});

describe('Bicicleta.findById',() => {
  it('Debe devolver la bici con id 1', () => {
    expect(Bicicleta.allBicis.length).toBe(0);
    var abici = new Bicicleta(1, "verde", "urbana");
    var abici2 = new Bicicleta(2, "rojo", "montaña");
    Bicicleta.add(abici);
    Bicicleta.add(abici2);

    var targetBici = Bicicleta.findById(1);
    expect(targetBici.id).toBe(1);
    expect(targetBici.color).toBe(abici.color);
    expect(targetBici.modelo).toBe(abici.modelo);
  });
});

// Hacer el remove
